namespace UpdateFetcher.Regions;

public interface IRegion
{
    SupportedRegion RegionEnum { get; }
    string Code { get;  }
    Uri DownloadUri { get; }
    bool LongDateFormat { get;  }

    IEnumerable<string> KnownModelNames { get; }
    // Task<HttpContent?> FindModelNames();
    Task<IEnumerable<string>> FindModelNames(Brand brand = Brand.HM, string? token = null);
}