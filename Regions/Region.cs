using System.Text;
using System.Text.Json;
using static UpdateFetcher.Downloader;

namespace UpdateFetcher.Regions;

public sealed class Region : IRegion
{
    public SupportedRegion RegionEnum { get; }
    private string Name { get; }
    public string Code => RegionEnum == SupportedRegion.Au ? "NA" : Name;

    public bool LongDateFormat => RegionEnum == SupportedRegion.Kr;

    public Uri DownloadUri =>
        RegionEnum == SupportedRegion.Kr
            ? new Uri("http://avn01.speednavi.co.kr/avn/SmartUpdate/INF_Data/")
            : new Uri($"http://oem-{Name}upload.map-care.com/{Code.ToUpper()}_update_data/{Code.ToUpper()}_INF/");

    internal Region(SupportedRegion regionEnum)
    {
        RegionEnum = regionEnum;
        Name = regionEnum.ToString();
    }

    public IEnumerable<string> KnownModelNames
    {
        get
        {
            var models = new List<string>();
            Task.Run(async () =>
            {
                var token = GenerateToken();
                var hmModels = FindModelNames(Brand.HM, token);
                var kmModels = FindModelNames(Brand.KM, token);
                var gnModels = FindModelNames(Brand.GN, token);
                models.AddRange(await hmModels);
                models.AddRange(await kmModels);
                models.AddRange(await gnModels);
            }).Wait();
            return models;
        }
    }

    /// <summary>
    /// Connects to official servers and fetches the token as the official updater app
    /// </summary>
    /// <returns></returns>
    [Obsolete("This method should not be used as it's not necessary given we can generate the token locally.")]
    private async Task<string> GetToken()
    {
        /* OLD GENERATE TOKEN
        var uri = new Uri("https://apieu.map-care.com/api/GetGUIDV2");
        var body = new
        {
            D1 = Name
        };

        var data = new StringContent(JsonSerializer.Serialize(body), Encoding.UTF8, "application/json");
        var response = await SendPostAsync(uri, data);

        return response != null ? (await response.ReadAsStringAsync()).Split("|").Last().TrimEnd() : "";
        */

        return $"{Name.ToLower()}{DateTime.Now:yMMddhhmmssff}";
    }

    /// <summary>
    /// Generates the auth token that it's used by the official updater.
    /// </summary>
    /// <returns></returns>
    private string GenerateToken() => $"{Name.ToLower()}{DateTime.Now:yMMddhhmmssff}";

    public async Task<IEnumerable<string>> FindModelNames(Brand brand = Brand.HM, string? token = null)
    {
        string[] euRegions = {"Eu", "Ru", "Tr"};
        var apiUrl = "api";
        if (euRegions.Contains(Name))
        {
            apiUrl = "apieu";
        }
        var uri = new Uri($"https://{apiUrl}.map-care.com/api/ChkRegistV2");
        var body = new
        {
            D1 = "",
            D2 = brand.ToString(),
            D3 = "",
            D4 = Name,
            D5 = token ?? GenerateToken(),
            D6 = "U"
        };

        // Console.WriteLine(JsonSerializer.Serialize(body)); // debug

        var data = new StringContent(JsonSerializer.Serialize(body), Encoding.UTF8, "application/json");
        var response = await SendPostAsync(uri, data);

        if (response == null)
            return Array.Empty<string>();

        return (await response.ReadAsStringAsync())
            .Split("\n")
            .Where(line => line.Contains("$1|"))
            .SelectMany(col => col.Split("|").Where(record => record.Contains(".inf")))
            .Select(model => model.Replace($".inf$1", "").Replace($".inf$0", ""));
    }
}
