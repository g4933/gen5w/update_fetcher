using IniParser;
using Ionic.Zip;
using System.IO;

namespace UpdateFetcher;

public static class Downloader
{
    private static readonly HttpClient Client = new();

    private static async Task ExtractAsync(string file)
    {
        await Task.Run(() =>
        {
            using var zip = ZipFile.Read(file);
            foreach (var e in zip)
            {
                e.Attributes = FileAttributes.Normal;
                e.Extract(Path.GetDirectoryName(file), ExtractExistingFileAction.OverwriteSilently);
            }
        });

        try
        {
            var path = Path.GetDirectoryName(file) ?? throw new InvalidOperationException();
            var zipParts = Directory.GetFiles(path, $"{Path.GetFileNameWithoutExtension(file)}.*");
            foreach (var part in zipParts) File.Delete(part);
        }
        catch (Exception e)
        {
            Console.WriteLine("Failed trying to cleanup some of the zip files. Sorry!");
            Console.WriteLine(e);
        }

        Console.WriteLine($"Extraction attempt for {file} completed.");
    }

    private static async Task DownloadAsync(Uri uri, string destFileName)
    {
        // var httpContent = await FetchInfFileAsync(new Uri($"{rootUrl}/{fileInfoPath}/{fileName}"));
        var httpContent = await DownloadFileAsync(uri);

        if (httpContent != null)
        {
            // var destFileName = $"{destDirModel}/{fileInfoPath}/{fileName}";
            Directory.CreateDirectory(Path.GetDirectoryName(destFileName) ?? throw new InvalidOperationException());
            await using var fs = new FileStream(destFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                FileShare.ReadWrite);
            fs.SetLength(0);
            await httpContent.CopyToAsync(fs);
            if (httpContent.Headers.LastModified != null)
                File.SetLastWriteTime(destFileName, httpContent.Headers.LastModified.Value.DateTime);
        }
    }

    public static async Task<HttpContent?> DownloadFileAsync(Uri uri)
    {
        try
        {
            var response = await Client.GetAsync(uri);
            return !response.IsSuccessStatusCode ? null : response.Content;
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine("\nException Caught!");
            Console.WriteLine("Message :{0} ", e.Message);
            return null;
        }
    }

    public static async Task DownloadUpdate(string fileInf, string dest)
    {
        var parser = new FileIniDataParser();
        parser.Parser.Configuration.CommentString = "#";
        parser.Parser.Configuration.CaseInsensitive = true;
        var inf = await parser.ReadLgeInfAsync(fileInf);

        // var downloadPrefix = inf.Sections["ENVIRONMENT"]["DOWNLOAD_PREFIX"].Replace("\"", "");
        var rootUrl = inf.Sections["ENVIRONMENT"]["ROOT_URL"].Replace("\"", "");
        var modelPrefix = inf.Sections["ENVIRONMENT"]["MODEL_PREFIX"].Replace("\"", "");
        var destDirModel =
            $"{dest}/{Path.GetFileNameWithoutExtension(fileInf)}/{rootUrl.Split('/').Last()}/{modelPrefix}";

        var throttler = new SemaphoreSlim(initialCount: 5);
        await Task.WhenAll(inf.Sections.Select(async section =>
        {
            if (!section.SectionName.Contains("_FILE_"))
                return;

            var fileName = section.Keys["AG_FILENAME"].Replace("\"", "");
            var zipName = section.Keys["AG_ZIPNAME"].Replace("\"", "");
            var fileInfoPath = section.Keys["FILEINFO_PATH"].Replace("\"", "")
                .Replace("$MODEL_PREFIX$", modelPrefix);
            var numSpan = int.Parse(section.Keys["AG_NUMSPAN"].Replace("\"", ""));

            try
            {
                await throttler.WaitAsync();
                if (numSpan == 0)
                    await DownloadZipAsync(rootUrl, fileInfoPath, fileName, destDirModel);
                else
                    await DownloadZipAsync(numSpan, rootUrl, fileInfoPath, zipName, destDirModel);
            }
            finally
            {
                throttler.Release();
            }
        }));
    }

    private static async Task DownloadZipAsync(int numSpan, string rootUrl, string fileInfoPath, string zipName,
        string dest)
    {
        var throttler = new SemaphoreSlim(initialCount: 3);
        await Task.WhenAll(Enumerable.Range(1, numSpan).Select(async i =>
        {
            await throttler.WaitAsync();
            try
            {
                var uri = new Uri($"{rootUrl}/{fileInfoPath}/{zipName.Replace(".ZIP", $"{i:000}.ZIP")}");
                var destFileName = numSpan == i
                    ? $"{dest}/{fileInfoPath}/{zipName}"
                    : $"{dest}/{fileInfoPath}/{zipName.Replace(".ZIP", $".z{i:00}")}";
                Console.WriteLine($"Fetching multipart ({i}/{numSpan}) from {uri}");
                await DownloadAsync(uri, destFileName);
            }
            finally
            {
                throttler.Release();
            }

            return i;
        }));
        if (File.Exists($"{dest}/{fileInfoPath}/{zipName}"))
        {
            try
            {
                Console.WriteLine($"Finished downloading [{dest}/{fileInfoPath}/{zipName}]... Will try to extract now!.");

                await ExtractAsync($"{dest}/{fileInfoPath}/{zipName}");
            }
            catch (Exception)
            {
                Console.WriteLine(
                    $"Failed trying to extract [{dest}/{fileInfoPath}/{zipName}]... You might wanna take a look yourself.");
                throw;
            }
        }
    }

    private static async Task DownloadZipAsync(string rootUrl, string fileInfoPath, string fileName, string dest)
    {
        var uri = new Uri($"{rootUrl}/{fileInfoPath}/{fileName}");
        var destFileName = $"{dest}/{fileInfoPath}/{fileName}";
        Console.WriteLine($"Fetching from {uri}");
        await DownloadAsync(uri, destFileName);
    }

    public static async Task<HttpContent?> SendPostAsync(Uri uri, StringContent data)
    {
        try
        {
            var response = await Client.PostAsync(uri, data);
            return !response.IsSuccessStatusCode ? null : response.Content;
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine("\nException Caught!");
            Console.WriteLine("Message :{0} ", e.Message);
            return null;
        }
    }
}
