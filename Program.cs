using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Reflection.Metadata;
using System.Security.Policy;
using System.Xml.Linq;
// using System.IO.Compression;
using UpdateFetcher.Regions;
using static UpdateFetcher.Downloader;

namespace UpdateFetcher;

public static class Program
{
    private static IRegion Region { get; set; } = null!;
    private static string Model { get; set; } = "2020_21_Ioniq_Plug_in_Hybrid_EU";


    private static async Task<int> Main(string[] args)
    {
        var rootCommand = new RootCommand("Update fetcher for hyundai / kia");
        var regionOption = new Option<SupportedRegion>(
            name: "--region",
            description: "The region to grab the info from.")
            { IsRequired = true };

        var modelOption = new Option<string>(
            name: "--model",
            description: "Car model to fetch.")
            { IsRequired = true };

        var infOption = new Option<string>(
                name: "--inf",
                description: "Path to inf file.")
            { IsRequired = true };

        var destOption = new Option<string>(
            getDefaultValue: () => "./downloads/updates/",
            name: "--dest",
            description: "Destination path to save the files.");

        var infCommand = new Command("inf", "Fetches all the infs available for the chosen model.")
        {
            regionOption,
            modelOption
        };
        infCommand.SetHandler(async (region, model) => await FindInfFiles(region, model, false), regionOption, modelOption);
        rootCommand.AddCommand(infCommand);

        var downloadCommand = new Command("download", "Downloads a chosen update.")
        {
            infOption,
            destOption
        };

        downloadCommand.SetHandler(async (inf, dest) => await Download(inf, dest), infOption, destOption);
        rootCommand.AddCommand(downloadCommand);

        var listCommand = new Command("list", "Lists known model names.")
        {
            regionOption
        };
        listCommand.SetHandler((command) => ListModels(command, false), regionOption);
        rootCommand.AddCommand(listCommand);

        var wizCommand = new Command("wizard", "Launch the helper wizard."){};

        wizCommand.SetHandler(async () => await StartWizard());
        rootCommand.AddCommand(wizCommand);

        if (args.Length < 1)
        {
            await StartWizard();
        }

        return await rootCommand.InvokeAsync(args);
    }

    private static async Task StartWizard()
    {
        string[] RegionsArr = Enum.GetNames(typeof(SupportedRegion));

        for (int i = 0; i < RegionsArr.Length; i++)
        {
            Console.WriteLine($"({i}) {RegionsArr[i]}");
        }
        Console.Write("\nPlease enter your region number: ");

        if (!Int32.TryParse(Console.ReadLine(), out int ChosenRegion))
        {
            Console.WriteLine("Invalid Choice");
            await StartWizard();
        }
        if (ChosenRegion < 0 || ChosenRegion > RegionsArr.Length - 1)
        {
            Console.WriteLine("Invalid Choice");
            await StartWizard();
        }
        Console.Clear();
        Console.WriteLine("Loading..");
        await ListModels((SupportedRegion) Enum.Parse(typeof(SupportedRegion), RegionsArr[ChosenRegion]), true);
    }

    private static async Task ListModels(SupportedRegion requestedRegion, Boolean wizard = false)
    {
        var region = GetRegion(requestedRegion);
        string[] ModelsArr = region.KnownModelNames.Cast<string>().ToArray();
        //Console.Clear();

        for (int i = 0; i < ModelsArr.Length; i++)
        {
            Console.WriteLine($"({i}) {ModelsArr[i]}");
        }

        if (wizard)
        {
            Console.Write("\nPlease enter your model number: ");

            if (!Int32.TryParse(Console.ReadLine(), out int ChosenModel))
            {
                Console.WriteLine("Invalid Choice");
                await ListModels(requestedRegion, wizard);
            }
            if (ChosenModel < 0 || ChosenModel > ModelsArr.Length - 1)
            {
                Console.WriteLine("Invalid Choice");
                await ListModels(requestedRegion, wizard);
            }

            Console.Clear();
            Console.WriteLine("Loading..");
            await FindInfFiles(region, ModelsArr[ChosenModel], true);
        }
    }

    private static async Task Download(string infFile, string dest)
    {
        await DownloadUpdate(infFile, dest);
    }


    private static async Task FindInfFiles(SupportedRegion requestedRegion, string requestedModel, Boolean wizard = false)
    {
        var region = GetRegion(requestedRegion);
        await FindInfFiles(region, requestedModel, wizard);
    }

    private static IRegion GetRegion(SupportedRegion requestedRegion) => new Region(requestedRegion);

    private static async Task FindInfFiles(IRegion region, string requestedModel, Boolean wizard = false)
    {
        var model = region.KnownModelNames.SingleOrDefault(known => known.Contains(requestedModel));
        if (model == null)
        {
            Console.WriteLine($"Warning: [{requestedModel}] is not on the list of known models for [{region.Code}]. We will still try it.");
            model = requestedModel;
        }

        List<string> infOptions = new();

        var years = new List<int>(Enumerable.Range(18, 6));
        var months = new List<int>(Enumerable.Range(1, 12));
        var revs = new List<char>(Enumerable.Range('A', 26).Select(x => (char)x));
        var quarters = new List<int>(Enumerable.Range(1, 4));

        var possibleVersions =
            (from year in years
             from month in months
             from rev in revs
             select $"{(region.LongDateFormat ? "20" : "")}{year}{month:D2}{rev}")
                .Concat(from year in years
                        from quarter in quarters
                        select $"{year}Q{quarter}").ToArray();

        await Task.WhenAll(possibleVersions.Select(async v =>
        {
            
            var uri = new Uri(region.DownloadUri, $"{v}/{model}.inf");
            var httpContent = await DownloadFileAsync(uri);
            if (httpContent != null)
            {
                var fileName = $"downloads/{requestedModel}/{v}_{model}.inf";
                Directory.CreateDirectory(Path.GetDirectoryName(fileName) ?? throw new InvalidOperationException());
                await using var fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                    FileShare.ReadWrite);
                fs.SetLength(0);
                await httpContent.CopyToAsync(fs);
                if (httpContent.Headers.LastModified != null) { 
                    File.SetLastWriteTime(fileName, httpContent.Headers.LastModified.Value.DateTime);
                }

                if (wizard)
                {
                    infOptions.Add(fileName);
                }
                else
                {
                    Console.WriteLine($"Fetched from {uri} | Stored at {fileName}");
                }
            }
        }));

        if (wizard)
        {
            Console.Clear();
            string[] infArr = infOptions.ToArray();
            for (int i = 0; i < infArr.Length; i++)
            {
                Console.WriteLine($"({i}) {infArr[i].Split("/").Last().Replace(".inf", "")}");
            }
            Console.Write("\nPlease enter a revision number: ");

            if (!Int32.TryParse(Console.ReadLine(), out int ChosenRev))
            {
                Console.WriteLine("Invalid Choice");
                await FindInfFiles(region, requestedModel, true);
            }
            if (ChosenRev < 0 || ChosenRev > infArr.Length - 1)
            {
                Console.WriteLine("Invalid Choice");
                await FindInfFiles(region, requestedModel, true);
            }

            await DownloadUpdate(infArr[ChosenRev], "./downloads/updates/");
        }
        
    }
}
